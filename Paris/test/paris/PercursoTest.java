/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import graphbase.Graph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ana Sofia
 * @author Tiago Ferreira
 */
public class PercursoTest {
    
    Graph<Estacao, Linha> grafoTestes = new Graph(true);
    
    Percurso percurso = new Percurso();

    Map<Estacao, Double> mapaEstacao1 = new LinkedHashMap<>();
    Map<Estacao, Double> mapaEstacao2 = new LinkedHashMap<>();
    
    Map<Linha, Map<Estacao, Double>> mapaLinha = new LinkedHashMap<>();
    
    Linha l1 = new Linha("1");
    Linha l2 = new Linha("2");
    
    Estacao e1 = new Estacao("Argentine", null);
    Estacao e2 = new Estacao("Charles de Gaulle Etoile", null);
    Estacao e3 = new Estacao("Termes", null);
    
    LinkedList<Estacao> lista = new LinkedList<>();
    
    public PercursoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        grafoTestes.insertEdge(e1, e2, l1, 1);
        grafoTestes.insertEdge(e2, e3, l2, 1);
        
        percurso.getListaEstacoes().add(e1);
        percurso.getListaEstacoes().add(e2);
        percurso.getListaEstacoes().add(e3);
        
        e1.getListaLinhas().add(l1);
        e2.getListaLinhas().add(l1);
        e2.getListaLinhas().add(l2);
        e3.getListaLinhas().add(l2);
        
        mapaEstacao1.put(e1, 0.0);
        mapaEstacao2.put(e2, 1.0);
        mapaEstacao2.put(e3, 2.0);
        mapaLinha.put(l1, mapaEstacao1);
        mapaLinha.put(l2, mapaEstacao2);
        
        lista.add(e1);
        lista.add(e2);
        lista.add(e3);
    }
    
    @After
    public void tearDown() {
    }

    

    /**
     * Test of constroiMapaPercurso method, of class Percurso.
     */
    @Test
    public void testConstroiMapaPercurso() {
        System.out.println("constroiMapaPercurso");
        Map<Linha, Map<Estacao, Double>> result = percurso.constroiMapaPercurso(grafoTestes);
        assertEquals(mapaLinha, result);
    }

    /**
     * Test of procuraEstacaoLinhaPorInstante method, of class Percurso.
     */
    @Test
    public void testProcuraEstacaoLinhaPorInstante() {
        System.out.println("procuraEstacaoLinhaPorInstante");
        double instanteTempo = 1.0;
        List<Estacao> listaInstante = new ArrayList<>();
        List<Estacao> expResult = listaInstante;
        List<Estacao> result = percurso.procuraEstacaoLinhaPorInstante(instanteTempo);
        assertEquals(expResult, result);
    
    }

    /**
     * Test of transfereEstacoesLista method, of class Percurso.
     */
    @Test
    public void testTransfereEstacoesLista() {
        System.out.println("transfereEstacoesLista");
        LinkedList<Estacao> expected = new LinkedList<>();
        expected.add(e1);
        expected.add(e2);
        expected.add(e3);
        percurso.transfereEstacoesLista(lista);
        assertEquals(lista, expected);
    }
    
}
