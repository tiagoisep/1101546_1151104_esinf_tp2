/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Ana Sofia
 * @author Tiago Ferreira
 */
public class Estacao {
    /**
     * atributo de estacao: nome de estacao
     */
    private final String nomeEstacao;
    
    /**
     *  atributo de estacao: coordenadas
     */
    private final Coordenadas coordenadas ;
    
    /**
     * atributo de estação: lista e linhas a que esta estação pertence
     */
      private Set<Linha> listaLinhas;
            
    
    /**
     * Constrói uma instância completa de Estação: recebe por argumento o nome
     * da estação e as suas coordenadas
     * recebe por atributo:
     * @param nomeEstacao, nome da estação
     * @param coordenadas , coordenadas da estação
     */
       public Estacao(String nomeEstacao, Coordenadas coordenadas){
        this.nomeEstacao = nomeEstacao;
        this.coordenadas = coordenadas;
        listaLinhas= new HashSet<>();
    }

    /**
     * Método que devolve o nome da estação.
     * 
     * @return the nomeEstacao
     */
    public String getNomeEstacao() {
        return nomeEstacao;
    }

    /**
     * 
     * Método que devolve um objeto do tipo coordenadas - as coordenadas da 
     * estação.
     * @return o objeto coordenadas
     */
    public Coordenadas getCoordenadas() {
        return coordenadas;
    }
    
    /**
     * Método que devolve um set de linhas - linhas a que a estação em 
     * questão pertence.
     * 
     * @return a lista de linhas a que a estação pertence
     */
    public Set<Linha> getListaLinhas(){
        return listaLinhas;
    }

    /**
     * Método que devolve uma string com o nome da estação em questão
     * 
     * @return o nome da estação 
     */
    @Override
    public String toString() {
        return nomeEstacao;
    }
    
    
            
    }
