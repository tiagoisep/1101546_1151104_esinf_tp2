/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import graphbase.Edge;
import graphbase.Graph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @author Ana Sofia
 * @author Tiago Ferreira
 */
public class Percurso {

    /**
     * Atributo de percurso, lista de estações do Percurso
     */
    private LinkedList<Estacao> listaEstacoes;
    
    /**
     * Atributo de percurso, mapa do Percurso 
     *  keys - linha
     *  Values - estação | instante de tempo
     */
    private Map<Linha, Map<Estacao, Double>> mapaPercurso;
    
    /**
     * Construtor completo: constói uma instancia de Percurso
     */
    public Percurso() {
        listaEstacoes = new LinkedList<Estacao>();
        mapaPercurso = new LinkedHashMap<>();
    }
    
    /**
     * Método que devolve a lista de estações do percurso. 
     * 
     * @return Lista de estações do percurso
     */
    public LinkedList<Estacao> getListaEstacoes() {
        return listaEstacoes;
    }
    
    /**
     * Método que devolve o mapa que representa o percurso
     * 
     * @return 
     */
    public Map<Linha, Map<Estacao, Double>> getMapaPercurso() {
        return mapaPercurso;
    }

    /**
     * Método que constrói o mapa do percurso: nas keys do mapa estão as linhas
     * que percorremos naquele percurso e nos values temo um outro mapa que tem
     * nas keys as estações por onde passamos no percurso e nos values o instante
     * de tempo em que isso aocntece. 
     * 
     * @param metro
     * @return mapa do percurso
     */
    public Map<Linha, Map<Estacao, Double>> constroiMapaPercurso(Graph metro) {

        double instanteTempo = 0;

        ListIterator<Estacao> it = listaEstacoes.listIterator();
        Estacao estacaoOrigem = it.next();
        Estacao estacaoDestino = it.next();
        Edge conexaoPercorrida = metro.getEdge(estacaoOrigem, estacaoDestino);
        Linha linhaPercorrida = (Linha) conexaoPercorrida.getElement();
        Map<Estacao, Double> mapaEstacaoTempo = new LinkedHashMap<>();
        mapaEstacaoTempo.put(estacaoOrigem, instanteTempo);
        mapaPercurso.put(linhaPercorrida, mapaEstacaoTempo);
        int cont = 0;

        while (it.hasNext() && cont < listaEstacoes.size() - 1) {
            estacaoOrigem = estacaoDestino;
            estacaoDestino = it.next();
            conexaoPercorrida = metro.getEdge(estacaoOrigem, estacaoDestino);
            linhaPercorrida = (Linha) conexaoPercorrida.getElement();
            instanteTempo = instanteTempo + conexaoPercorrida.getWeight();

            if (mapaPercurso.containsKey(linhaPercorrida)) {
                mapaPercurso.get(linhaPercorrida).put(estacaoOrigem, instanteTempo);

            } else {
                Map<Estacao, Double> novoMapaEstacaoTempo = new LinkedHashMap<>();
                novoMapaEstacaoTempo.put(estacaoOrigem, instanteTempo);
                mapaPercurso.put(linhaPercorrida, novoMapaEstacaoTempo);

            }
            cont++;

        }

        instanteTempo = instanteTempo + conexaoPercorrida.getWeight();

        if (mapaPercurso.containsKey(linhaPercorrida)) {
            mapaPercurso.get(linhaPercorrida).put(estacaoDestino, instanteTempo);

        } else {
            Map<Estacao, Double> novoMapaEstacaoTempo = new LinkedHashMap<>();
            novoMapaEstacaoTempo.put(estacaoDestino, instanteTempo);
            mapaPercurso.put(linhaPercorrida, novoMapaEstacaoTempo);

        }

        return mapaPercurso;
    }

    
    /**
     * Método que devolve ente que estações se encontra um utilizador da linha do
     * metro num determinado percurso, dado um instante de tempo que é recebido 
     * por parâmetro.
     * 
     * @param instanteTempo
     * @return lista de estações
     */
    public List<Estacao> procuraEstacaoLinhaPorInstante(double instanteTempo) {
        List<Estacao> betweenStations = new ArrayList<>();
        Estacao primeiraEstacao = null;

        for (Map.Entry<Linha, Map<Estacao, Double>> entry : mapaPercurso.entrySet()) {
            Linha key = entry.getKey();
            Map<Estacao, Double> value = entry.getValue();
            for (Map.Entry<Estacao, Double> entry1 : value.entrySet()) {
                Estacao key1 = entry1.getKey();
                Double value1 = entry1.getValue();
                if (value1 == instanteTempo) {
                    betweenStations.add(key1);
                    return betweenStations;
                }
                if (value1 < instanteTempo) {
                    primeiraEstacao = key1;
                } else {
                    betweenStations.add(primeiraEstacao);
                    betweenStations.add(key1);
                    return betweenStations;
                }

            }

        }
        return betweenStations;
    }
    
    public void transfereEstacoesLista(LinkedList<Estacao> listaEstacoesParciais){
        for (Estacao estacao : listaEstacoesParciais) {
            if(!listaEstacoes.contains(estacao))
                listaEstacoes.add(estacao);
        }
    }

}
