/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

/**
 * @author Ana Sofia
 * @author Tiago Ferreira
 */
public class Coordenadas {
    /**
     * Latitude, atributo de Coordenadas
     */
    private double latitude;
    
    /**
     * Longitude, atributo de Coordenadas
     */
    private double longitude;
    
    /**
     * Contrutor completo: contrói uma instância de Coordenadas rebebendo por
     * parâmetro a latitude e a longitude. 
     * @param latitude
     * @param longitude 
     */
    public Coordenadas(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Método que devolve a latitude de uma estação
     * @return latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Método que devolve a longitude de uma estação
     * @return longitude
     */
    public double getLongitude() {
        return longitude;
    }
    
    /**
     * Método que devolve uma string com a informação da latitude e longitude da
     * estação
     * 
     * @return string com a latitude e longitude
     */
    @Override
    public String toString() {
        return "latitude = " + latitude + "º, longitude = " + longitude + "º";
    }
}
