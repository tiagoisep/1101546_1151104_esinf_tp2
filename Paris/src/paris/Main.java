/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Ferreira
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {

        //Alinea 1
        Graph<Estacao, Linha> grafoEstacoes = new Graph<>(true);

        Set<Estacao> listaEstacoesGlobal = new HashSet<>();

        LerFicheiro.lerFicheiros(grafoEstacoes, listaEstacoesGlobal);
        
        
        
        //Alinea 2
        System.out.println("\n##### GRAFO CONEXO? #####");

        Estacao e1 = procuraEstacao(listaEstacoesGlobal, "La Defense");
        Estacao e2 = procuraEstacao(listaEstacoesGlobal, "Termes");
        Estacao e3 = procuraEstacao(listaEstacoesGlobal, "Nation");
  
        LinkedList<Estacao> listaEstacoesVisitadas = GraphAlgorithms.DepthFirstSearch(grafoEstacoes, e1);

        if (listaEstacoesVisitadas.size() == listaEstacoesGlobal.size()) {
            System.out.println("Conexo");

        } else {
            System.out.println("Não conexo");
        }

        
        //Alinea 3 é a criação da classe percurso
        
        
        
        //Alinea 4
        System.out.println("\n##### Alinea 4 - Caminho mínimo pelo menor Nº de Estações #####");
        Percurso percurso = new Percurso();
        double distancia = GraphAlgorithms.shortestPathEstacao(grafoEstacoes, e2, e3, percurso.getListaEstacoes());
        Map<Linha, Map<Estacao, Double>> mapaCaminho = percurso.constroiMapaPercurso(grafoEstacoes);
        imprimePercurso(mapaCaminho);

        
        //Alinea 5
        System.out.println("\n##### Alinea 5 - Caminho mais rápido #####");
        double tempo = GraphAlgorithms.shortestPath(grafoEstacoes, e1, e2, percurso.getListaEstacoes());
        mapaCaminho = percurso.constroiMapaPercurso(grafoEstacoes);
        imprimePercurso(mapaCaminho);

        List<Estacao> estacoes = percurso.procuraEstacaoLinhaPorInstante(10f);
        if (estacoes.isEmpty()) {
            System.out.println("O utilizador já percorreu todo o percurso para o instante de tempo mencionado.");
        }
        if (estacoes.size() == 1) {
            System.out.println("No instante de tempo mencionado o utilizdor está em " + estacoes.get(0));
        }
        if (estacoes.size() == 2) {
            System.out.print("O utlizador está entre as estações: ");
            System.out.println(estacoes.get(0) + " e " + estacoes.get(1) + ".");
        }
        System.out.println("");
        

        //Alinea 6
        System.out.println("\n##### Alinea 6 - Caminho minimo entre 2 estações com menor mudanças de linha #####");
        Graph<Estacao, Linha> graphCloned = grafoEstacoes.clone();
        percurso = new Percurso();
        double comp = GraphAlgorithms.shortestPathMudancaLinha(graphCloned, e2, e3, percurso.getListaEstacoes());
        mapaCaminho = percurso.constroiMapaPercurso(grafoEstacoes);
        imprimePercurso(mapaCaminho);

     
        //Alínea 7
        Estacao e5 = LerFicheiro.procurarEstacaoPorNome(listaEstacoesGlobal, "Les Sablons");
        Estacao e6 = LerFicheiro.procurarEstacaoPorNome(listaEstacoesGlobal, "Charles de Gaulle Etoile");
        Percurso p = new Percurso();
        
        constroiPercursoGlobal(grafoEstacoes, e1, e5, p);
        constroiPercursoGlobal(grafoEstacoes, e5, e6, p);
        constroiPercursoGlobal(grafoEstacoes, e6, e2, p);
 
        System.out.println("\n##### Alínea 7 - Caminho minimo entre 2 estações para um dado instante inicial e havendo estações intermédias #####");
        mapaCaminho = p.constroiMapaPercurso(grafoEstacoes);
        imprimePercurso(mapaCaminho);
    }
    
    /**
    /**
    /**
     * Método que procura uma estação pelo nome
     * 
     * @param listaEstacoesGlobal
     * @param nome
     * @return a estação procurada ou null se não existir/não encontrar
     */
    private static Estacao procuraEstacao(Set<Estacao> listaEstacoesGlobal, String nome){
        for (Estacao estacao : listaEstacoesGlobal) {
            if(estacao.getNomeEstacao().equalsIgnoreCase(nome)){
                return estacao;
            }
        }
        return null;
    }
    
    /**
     * Método que permite imprimir o Percurso do Metro entre 2 estações
     * 
     * @param mapaCaminho 
     */
    private static void imprimePercurso(Map<Linha, Map<Estacao, Double>> mapaCaminho){
        System.out.println("\nPercurso:\nLinha\t\tEstação\t\tInstante de Tempo");
        for (Map.Entry<Linha, Map<Estacao, Double>> entry : mapaCaminho.entrySet()) {
            Linha key = entry.getKey();
            System.out.println(key);
            Map<Estacao, Double> value = entry.getValue();
            for (Map.Entry<Estacao, Double> entry1 : value.entrySet()) {
                Estacao key1 = entry1.getKey();
                Double value1 = entry1.getValue();
                System.out.printf("\t%-35s%.1f\n", key1, value1);
            }

        }
    }

    /**
     * Método que constrói o percurso global tendo em conta vários percursos parciais
     * 
     * @param grafoEstacoes
     * @param e1
     * @param e2
     * @param p 
     */
    private static void constroiPercursoGlobal(Graph<Estacao, Linha> grafoEstacoes, Estacao e1, Estacao e2, Percurso p){
        LinkedList<Estacao> listaEstacoesPercursoParcial = new LinkedList<>();
        double tempo1 = GraphAlgorithms.shortestPath(grafoEstacoes, e1, e2, listaEstacoesPercursoParcial);
        p.transfereEstacoesLista(listaEstacoesPercursoParcial);
    }
    
}
