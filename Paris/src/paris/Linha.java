/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import java.util.Objects;

/**
 * @author Ana Sofia
 * @author Tiago Ferreira
 *
 */
public class Linha {

    /**
     * atributo de Linha codLinha
     */
    private final String codLinha;
  
    /**
     * Constrói uma instancia de linha, recebe como por parâmetro apenas o código
     * da linha. 
     *
     * @param codLinha
     */
    public Linha(String codLinha) {
        this.codLinha = codLinha;
    }

    /**
     * Método que devolve o código da linha. 
     * 
     * @return the codLinha
     */
    public String getCodLinha() {
        return codLinha;
    }

    @Override
    public String toString() {
        return codLinha ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.codLinha);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linha other = (Linha) obj;
        
        return this.codLinha.equalsIgnoreCase(other.getCodLinha());
       
    }

    
    

}
