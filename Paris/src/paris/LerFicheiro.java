/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paris;

import graphbase.Edge;
import graphbase.Graph;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Ana Sofia
 * @author Tiago Ferreira
 */
public class LerFicheiro {

    private static final String SEPARADOR = ";";
    
    /**
     * Método que chama os métodos de ler ficheiro para cada um dos três 
     * ficheiros de dados.
     * 
     * @param grafo
     * @param listaEstacoes
     * @throws FileNotFoundException 
     */
    public static void lerFicheiros(Graph<Estacao, Linha> grafo, Set<Estacao> listaEstacoes) throws FileNotFoundException{
        lerEstacoes(listaEstacoes);
        lerLinhasEstacoes(listaEstacoes);
        lerConnections(grafo, listaEstacoes);
    }
    
    /**
     * Método que faz a leitura do ficheiro "coordinates.cvs" e que contrói um
     * set de estações.
     * 
     * @param listaEstacoes
     * @throws FileNotFoundException 
     */
    public static void lerEstacoes(Set<Estacao> listaEstacoes) throws FileNotFoundException {
        Scanner lerCoordenadas = new Scanner(new File("coordinates.csv"));

        String lido;                                                
        String[] campo;
        while (lerCoordenadas.hasNext()) {
            lido = lerCoordenadas.nextLine();
            campo = lido.split(SEPARADOR);
            String nome = campo[0];
            double lat = Double.parseDouble(campo[1]);
            double lon = Double.parseDouble(campo[2]);
            Coordenadas c = new Coordenadas(lat, lon);
            Estacao e = new Estacao(nome, c);
            listaEstacoes.add(e);
        }
    }

    /**
     * Método que faz a leitura do ficheiro "lines_and_stations.csv" e permite 
     * adicionar às estações as diferentes linhas a que ela pertence.
     * 
     * @param listaEstacoes
     * @throws FileNotFoundException 
     */
    public static void lerLinhasEstacoes(Set<Estacao> listaEstacoes) throws FileNotFoundException {

        Scanner lerLinhasEstacoes = new Scanner(new File("lines_and_stations.csv"));

        String lido;
        String[] campo;
        while (lerLinhasEstacoes.hasNext()) {
            lido = lerLinhasEstacoes.nextLine();
            campo = lido.split(SEPARADOR);
            Linha linha = new Linha(campo[0]);
            String nome = campo[1];
            Estacao est = procurarEstacaoPorNome(listaEstacoes, nome);
            
            if(est != null){
                est.getListaLinhas().add(linha);
                        
            }
        }
    }

    /**
     * Método que faz a leitura do ficheiro connections.csv e que contrói o grafo
     * representativo do metro de Paris.
     * 
     * @param grafo
     * @param listaEstacoes
     * @throws FileNotFoundException 
     */
    public static void lerConnections(Graph<Estacao, Linha> grafo, Set<Estacao> listaEstacoes) throws FileNotFoundException {

        Scanner lerConnections = new Scanner(new File("connections.csv"));

        String lido;
        String[] campo;
        while (lerConnections.hasNext()) {
            lido = lerConnections.nextLine();
            campo = lido.split(SEPARADOR);

            Linha linha = new Linha(campo[0]);
            String nome1 = campo[1];
            String nome2 = campo[2];
            int tempo = Integer.parseInt(campo[3]);

            Estacao estacao1 = procurarEstacaoPorNome(listaEstacoes, nome1);
            Estacao estacao2 = procurarEstacaoPorNome(listaEstacoes, nome2);

            grafo.insertEdge(estacao1, estacao2, linha, tempo);
           
            
        }
    }

    /**
     * Método que permite procurar uma estação no Set de estações a partir do seu
     * nome. 
     * 
     * @param listaEstacoes
     * @param nome
     * @return estação procurada
     */
    public static Estacao procurarEstacaoPorNome(Set<Estacao> listaEstacoes, String nome) {
        for (Estacao estacao : listaEstacoes) {
            if(estacao.getNomeEstacao().equalsIgnoreCase(nome)){
                return estacao;
            }
        }
      
        return null;
    }

}
